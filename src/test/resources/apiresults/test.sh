## Scopus

curl https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&retmode=json&retmax=20&sort=relevance&term=fever
curl https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&retmode=xml&id=26291402
curl -g https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&retmode=json&retmax=20&sort=relevance&term=bristol%20[AD]
curl -X GET --header Accept: application/json https://api.elsevier.com/content/search/scopus?query=affil(University%20of%20Bristol)&apiKey=7f59af901d2d86f78a1fd60c1bf9426a

# Fetch author info only for publication. It includes affiliation id.
curl -H 'Accept: application/json' 'https://api.elsevier.com/content/abstract/scopus_id/85011633079?field=author&apiKey=7f59af901d2d86f78a1fd60c1bf9426a'

# You can limit returned fields, although not sure how to limit to, e.g. author/afid
#
# More info:
#
# https://api.elsevier.com/documentation/SCOPUSSearchAPI.wadl (general search)
# http://api.elsevier.com/documentation/search/SCOPUSSearchViews.htm (views)
# http://api.elsevier.com/content/search/fields/scopus (search fields)

# Suggested search: af-id(60020650) or af-id(60014311) or ...
# for each of 60020650 60014311 60013426 60101499 (suggested by alex)

# This fetches UoB (60020650) stuff, includes title, authors, DOI, coverDate and id for outputs, and author info including affiliation id(s)
curl 'https://api.elsevier.com/content/search/scopus?query=af-id(60020650)&apiKey=7f59af901d2d86f78a1fd60c1bf9426a&startPage=1&field=title,doi,identifier,author,afid,coverDate,eid'  -H 'Accept: application/xml' 

# Hmm, there is a openaccessArticle field in full text
curl 'https://api.elsevier.com/content/article/eid/2-s2.0-85019401262' -H 'Accept: application/xml' 

## PURE

# search for item given a scopus id
curl 'https://ris-b.services.bris.ac.uk/ws/rest/publication.current?source.value=80051552440&source.name=Scopus'

# value can have multiple values
curl 'https://ris-b.services.bris.ac.uk/ws/rest/publication.current?source.name=Scopus&source.value=84995644649,85011872869'

# Author search in Pure. Scopus includes initials in given name, it seems. I strip them.
# then search for 'firstname:"given" lastname:"surname"'
# (note: we won't find students in this service)

curl -k 'https://ris-b.services.bris.ac.uk/ws/rest/person.current?searchString=firstname:%22Casimir%20%22%20lastname:%22Ludwig%22&rendering=xml_long'

## PubMed

# What fun!
# See http://www.fredtrotter.com/2014/11/14/hacking-on-the-pubmed-api/ for some pointers
# https://www.ncbi.nlm.nih.gov/pubmed/advanced was useful for building queries
# e.g. term=university of bristol[Affiliation]

# Search for Bristol affiliated things in pubmed (disable globbing)

curl -g 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term=university%20of%20bristol[Affiliation]'

# Fetch summary about result(s)

curl 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pubmed&id=28609582'

# Fetch full metadata (?) - affiliations and names are better

curl 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&retmode=xml&id=28614739,28614628'