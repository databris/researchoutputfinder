package uk.ac.bris.researchoutputfinder.dao.scopus;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import static org.junit.Assert.*;
import uk.ac.bris.researchoutputfinder.dao.scopus.ScopusOutput.FullTextWrapper;

/**
 *
 * @author pldms
 */
public class ScopusOutputResultTest {
    
    @Test
    public void testDeserialiseResults() throws IOException, JAXBException {
        InputStream in = Files.newInputStream(
                Paths.get("src/test/resources/apiresults", 
                        "scopus_output_and_author.xml"));
        
        JAXBContext context = JAXBContext.newInstance(ScopusOutputResult.class);
        
        Unmarshaller unmarshaller = context.createUnmarshaller();
        
        ScopusOutputResult r = (ScopusOutputResult) unmarshaller.unmarshal(in);
        
        assertThat("Right number of results", r.results().size(), is(25));
        
        assertThat("First result title is ok", 
                r.results().get(0).title,
                is("SUMOylation of FOXP1 regulates transcriptional repression via CtBP1 to drive dendritic morphogenesis"));
        
        assertThat("First result eid is ok", 
                r.results().get(0).eid,
                is("2-s2.0-85018178754"));
        
        assertThat("First result first author name ok", 
                r.results().get(0).authors.get(0).surname,
                is("Rocca"));
        
        assertThat("First result first author first affiliation ok", 
                r.results().get(0).authors.get(0).affiliationIds.get(0),
                is(60020650));
        
        assertThat("First result aggregation type is ok",
                r.results().get(0).aggregationType,
                is("Journal"));
        
        assertThat("First result publication name is ok",
                r.results().get(0).publicationName,
                is("Science of the Total Environment"));
    }
    
    @Test
    public void testDeserialiseFullText() throws IOException, JAXBException {
        InputStream in = Files.newInputStream(
                Paths.get("src/test/resources/apiresults", 
                        "scopus_full_text.xml"));
                
        JAXBContext context = JAXBContext.newInstance(FullTextWrapper.class);
        
        Unmarshaller unmarshaller = context.createUnmarshaller();
        
        FullTextWrapper r = (FullTextWrapper) unmarshaller.unmarshal(in);
        
        assertThat("Article is OA", r.output.openaccessArticle, is(true));
    }
    
}
