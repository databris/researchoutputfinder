package uk.ac.bris.researchoutputfinder.dao.pure;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class PurePersonResultTest {
    
    public PurePersonResultTest() {
    }

    @Test
    public void testDeserialiseResults() throws IOException, JAXBException {
        InputStream in = Files.newInputStream(
                Paths.get("src/test/resources/apiresults", 
                        "pure_person_search.xml"));
        
        JAXBContext context = JAXBContext.newInstance(PurePersonResult.class);
        
        Unmarshaller unmarshaller = context.createUnmarshaller();
        
        PurePersonResult result = (PurePersonResult) unmarshaller.unmarshal(in);
        
        System.out.println(result.results);
        
        assertThat(result.count, is(1));
        
        assertThat(result.results.size(), is(1));
        
        assertThat(result.results.get(0).firstName, is("Casimir"));
        
        assertThat(result.results.get(0).id, is("59f8bdcf-031c-4155-a775-2048517b838f"));
        
        assertNotNull(result.results.get(0).orgs);
        
        System.out.printf("Org: %s %s", result.results.get(0).orgs.get(0).email, result.results.get(0).orgs.get(0).name);
        
        assertThat(result.results.get(0).getAcademicOrg(), is("School of Experimental Psychology"));
        
        assertThat(result.results.get(0).getEmail(), is("C.Ludwig@bristol.ac.uk"));
    }
    
}
