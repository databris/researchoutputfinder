package uk.ac.bris.researchoutputfinder.dao.pubmed;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class PubMedSearchResultTest {
    
    @BeforeClass
    public static void init() {
        System.setProperty("javax.xml.accessExternalDTD", "all");
    }
    
    
    @Test
    public void testDeserialiseResults() throws IOException, JAXBException {
        InputStream in = Files.newInputStream(
                Paths.get("src/test/resources/apiresults", 
                        "pubmed_search.xml"));
        
        JAXBContext context = JAXBContext.newInstance(PubMedSearchResult.class);
        
        Unmarshaller unmarshaller = context.createUnmarshaller();
        
        PubMedSearchResult result = (PubMedSearchResult) unmarshaller.unmarshal(in);
                
        assertThat(result.count, is(22802));
                
        assertThat(result.ids.size(), is(20));
                
        assertThat(result.ids, hasItems(28608031,28608031));
    }
    
}
