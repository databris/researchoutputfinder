package uk.ac.bris.researchoutputfinder.dao.pure;

import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class PureServiceTest {
    
    /**
     * Test of stripInitials method, of class PureService.
     */
    @Test
    public void testStripInitials() {
        assertThat(PureService.stripInitials("Name A "), is("Name"));
        assertThat(PureService.stripInitials("Name A."), is("Name"));
        assertThat(PureService.stripInitials("Name AB"), is("Name"));
        assertThat(PureService.stripInitials("Name A. B."), is("Name"));
        assertThat(PureService.stripInitials("Name A.B."), is("Name"));
    }
    
}
