package uk.ac.bris.researchoutputfinder.dao.pure;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class PureOutputResultTest {
    
    public PureOutputResultTest() {
    }

    @Test
    public void testDeserialiseResults() throws IOException, JAXBException {
        InputStream in = Files.newInputStream(
                Paths.get("src/test/resources/apiresults", 
                        "pure_output_search.xml"));
        
        JAXBContext context = JAXBContext.newInstance(PureOutputResult.class);
        
        Unmarshaller unmarshaller = context.createUnmarshaller();
                
        PureOutputResult result = (PureOutputResult) unmarshaller.unmarshal(in);
        
        assertThat(result, notNullValue());
        
        assertThat(result.count, is(15));
        
        assertThat(result.results.size(), is(10));
        
        assertThat(result.results.get(0).externalId.sourceId, is("29290347"));
        
        assertThat(result.results.get(0).doi, is("http://dx.doi.org/10.1038/nature22040"));
        
        assertThat(result.results.get(0).id, is("974de1b8-66ef-4dfa-a852-4a9e498cc43d"));
    }
    
}
