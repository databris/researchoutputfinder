package uk.ac.bris.researchoutputfinder.dao.pubmed;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class PubMedFetchResultTest {
    
    @BeforeClass
    public static void init() {
        System.setProperty("javax.xml.accessExternalDTD", "all");
    }
    
    @Test
    public void testDeserialiseResults() throws IOException, JAXBException {
        InputStream in = Files.newInputStream(
                Paths.get("src/test/resources/apiresults", 
                        "pubmed_fetch_articles.xml"));
        
        JAXBContext context = JAXBContext.newInstance(PubMedFetchResult.class);
        
        Unmarshaller unmarshaller = context.createUnmarshaller();
        
        PubMedFetchResult result = (PubMedFetchResult) unmarshaller.unmarshal(in);
                
        assertThat(result.articles.size(), is(2));
                
        assertThat(result.articles.get(0).title, is("Assessment of soil water, carbon and nitrogen cycling in reseeded grassland on the North Wyke Farm Platform using a process-based model."));
        
        assertThat(result.articles.get(0).doi, is("10.1016/j.scitotenv.2017.06.012"));
        
        assertThat(result.articles.get(0).publicationType, is("Journal Article"));
        
        assertThat(result.articles.get(0).sourceTitle, is("The Science of the total environment"));
        
        assertThat(result.articles.get(1).authors.get(0).affilitation, is("University of Bristol, School of Chemistry, Cantock's Close, BS8 1TS, Bristol, UNITED KINGDOM."));
    }
    
}
