package uk.ac.bris.researchoutputfinder;

import com.opencsv.CSVWriter;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import static uk.ac.bris.researchoutputfinder.PubMed.PURE_PERSON_LINK;
import uk.ac.bris.researchoutputfinder.dao.pure.PureOutputResult;
import uk.ac.bris.researchoutputfinder.dao.pure.PurePerson;
import uk.ac.bris.researchoutputfinder.dao.pure.PureService;
import uk.ac.bris.researchoutputfinder.dao.scopus.ScopusAuthor;
import uk.ac.bris.researchoutputfinder.dao.scopus.ScopusOutput;
import uk.ac.bris.researchoutputfinder.dao.scopus.ScopusOutputResult;
import uk.ac.bris.researchoutputfinder.dao.scopus.ScopusService;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class Scopus {

    public static void main(String[] args) throws InterruptedException, IOException {
        
        Properties config = Config.load();
        
        // All UoB
        List<Integer> affiliations
                = Arrays.asList(60020650, 60014311, 60013426, 60101499);

        ScopusService ss = new ScopusService(
                URI.create(config.getProperty("scopus.endpoint")),
                config.getProperty("scopus.apikey")
        );

        PureService pure = new PureService(
                URI.create(config.getProperty("pure.endpoint")), //"https://research-information.bris.ac.uk/ws/api/510/"),
                config.getProperty("pure.apikey") //"2828b254-0b6d-4c37-8a2f-d54387e49da8"
        );

        Path outputFile = Paths.get("/tmp/scopus.csv");

        CSVWriter writer = new CSVWriter(Files.newBufferedWriter(outputFile));

        writer.writeNext(new String[]{"doi", "date", "title", "source type", "source title", "author", "affliliation",
            "pure author", "school", "pure url", "email"});
        
        String startDate = "20180409";
        
        int start = 0;
        int numFound;
        while (start < 1000) {
            System.out.printf("# Start %d\n", start);
            numFound = checkScopusPage(ss, pure, start, 25, affiliations, writer, startDate);
            System.out.printf("# Found %d\n", numFound);
            if (numFound == -1) {
                System.out.println("# All done!");
                break;
            } // done
            start += numFound;
        }

        writer.flush();
        writer.close();
    }

    public static int checkScopusPage(ScopusService ss, PureService pure,
            int start, int step, List<Integer> affiliations, CSVWriter writer,
            String startDate) throws InterruptedException, IOException {

        ScopusOutputResult results
                = ss.findOutputsWithAffiliationFromIndex(affiliations, 
                        start, step, startDate);
        
        if (results.results() == null) { // out of results
            return -1; // signal done
        }

        for (ScopusOutput result : results.results()) {
            
            if (result.coverDate.compareTo("2016-01-01") < 0) {
                // Skip ones that are earlier than 2016
                System.out.printf("< ");
            } else if (pure.findOutputById(result.getIdentifer(), "source") != null) {
                // Skip the ones already in pure
                System.out.print("+ ");
            } else {
                System.out.print("! ");

                List<ScopusAuthor> uobScopusAuthors = result.authors.stream()
                        .filter(auth -> !Collections.disjoint(auth.affiliationIds, affiliations))
                        //.map(auth -> pure.findPersonFromName(auth.givenName, auth.surname))
                        .collect(Collectors.toList());

                String[] row = new String[11];

                row[0] = result.getDOI();
                row[1] = result.coverDate;
                row[2] = result.title;
                row[3] = result.aggregationType;
                row[4] = result.publicationName;
                
                if (uobScopusAuthors.isEmpty()) {
                    System.err.println("No bristol authors for " + result.getDOI());
                    row[5] = "No UoB authors found (?)";
                }
                
                for (ScopusAuthor scopusAuthor : uobScopusAuthors) {

                    List<PurePerson> people = pure.findPersonFromName(scopusAuthor.givenName, scopusAuthor.surname);

                    if (people.isEmpty()) {
                        // we already have something a bit better, so skip it
                        if (row[7] != null) continue;
                        
                        row[5] = scopusAuthor.surname + ", " + scopusAuthor.givenName;
                        row[6] = scopusAuthor.affiliationIds.toString();
                        row[7] = "NOT FOUND";
                    } else if (people.size() > 1) {
                        // we already have something a bit better, so skip it
                        if (row[7] != null && !row[7].equals("NOT FOUND")) continue;
                    
                        row[5] = scopusAuthor.surname + ", " + scopusAuthor.givenName;
                        row[6] = scopusAuthor.affiliationIds.toString();
                        row[7] = "FOUND " + people.size() + " PEOPLE";
                        row[10] = people.stream()
                            .map(PurePerson::getEmail)
                            .collect(Collectors.joining(" , "));
                    } else {
                        row[5] = scopusAuthor.surname + ", " + scopusAuthor.givenName;
                        row[6] = scopusAuthor.affiliationIds.toString();
                        row[7] = people.get(0).getName();
                        row[8] = people.get(0).getAcademicOrg();
                        row[9] = String.format(PURE_PERSON_LINK, people.get(0).id);
                        row[10] = people.get(0).getEmail();
                        // This is as good as it gets - use this row
                        break;
                    }
                }
            
                writer.writeNext(row);
                writer.flush();
            }

            System.out.printf("(%s) %s\t%s\t%s\t%s\n",
                    result.coverDate,
                    result.getIdentifer(),
                    result.getDOI(),
                    "https://api.elsevier.com/content/article/eid/" + result.eid,
                    result.title);
        }

        return results.results().size();
    }
}
