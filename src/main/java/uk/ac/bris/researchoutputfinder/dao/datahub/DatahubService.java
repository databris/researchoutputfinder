package uk.ac.bris.researchoutputfinder.dao.datahub;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import uk.ac.bris.researchoutputfinder.dao.pure.PureOutput;
import static uk.ac.bris.researchoutputfinder.dao.pure.PureService.stripInitials;

/**
 *
 * @author pldms
 */
public class DatahubService {
    
    final static String PUBLICATION_PRESENT = 
"select 1\n" +
"from datahub.v_pure_publication \n" +
"where \n" +
"title = ? and\n" +
"rownum = 1";
    
    final static String PERSON_AND_ORG =
"select person.person_id, coalesce(known_as, first_name) || ' ' || surname as name,\n" +
"listagg(organisation_code, ', ') within group (order by organisation_code) as orgs \n" +
"from datahub.person, datahub.person_organisation \n" +
"where (first_name = ? or known_as = ?) and  surname = ?\n" +
"and person.person_id = person_organisation.person_id\n" +
"group by person.person_id, datahub.person.surname, first_name, known_as";
    
    private final Connection connection;
    
    public DatahubService() throws SQLException {
        
        this.connection = DriverManager.getConnection(
            "jdbc:oracle:thin:@ldap://ldap-srv.bris.ac.uk:389/DPROD,cn=OracleContext,dc=bris,dc=ac,dc=uk",
            "RES_REVEAL",
            "matthews776" );
        
        // Match strings linguistically (?)
        try (Statement stmt = connection.createStatement()) {
            stmt.execute("ALTER SESSION SET NLS_COMP=LINGUISTIC");
            stmt.execute("ALTER SESSION SET NLS_SORT=BINARY_CI");
        }
    }
    
    public void close() throws SQLException {
        connection.close();
    }
    
    public boolean titleInPure(String title, String doi) throws SQLException {
                
        try (PreparedStatement stmt = connection.prepareStatement(PUBLICATION_PRESENT)) {
            stmt.setString(1, title);
            ResultSet results = stmt.executeQuery();
            return results.next();
        }
        
    }

    public List<PersonAndOrg> findPersonFromName(String forename, String surname) throws SQLException {
        
        String forenameCleaned = stripInitials(forename);
        
        try (PreparedStatement stmt = connection.prepareStatement(PERSON_AND_ORG)) {
            stmt.setString(1, forenameCleaned);
            stmt.setString(2, forenameCleaned);
            stmt.setString(3, surname);
            ResultSet results = stmt.executeQuery();
            
            List<PersonAndOrg> res = new ArrayList<>(1);
            
            System.out.printf("Name query: '%s' (%s) '%s'\n", forename, forenameCleaned, surname);
            
            while (results.next()) {
                PersonAndOrg pao = new PersonAndOrg();
                pao.personId = results.getLong("person_id");
                pao.name = results.getString("name");
                pao.org = results.getString("orgs");
                res.add(pao);
            }
            
            System.out.printf("%d hits %s\n", res.size(), res);
            
            return res;
        }
    }
    
    public static void main(String[] args) throws SQLException {
        DatahubService service = new DatahubService();
        
        List<PersonAndOrg> r = service.findPersonFromName("david", "Matthews");
        
        service.close();
        
        System.out.println("Got: " + r);
    }

    public static class PersonAndOrg {
        public long personId;
        public String name;
        public String org;
        
        @Override
        public String toString() {
            return String.format("%d: %s (%s)", personId, name, org);
        }
    }
}
