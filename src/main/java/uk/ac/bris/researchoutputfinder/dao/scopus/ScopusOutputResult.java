package uk.ac.bris.researchoutputfinder.dao.scopus;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pldms
 */
@XmlRootElement(name = "atom:search-results")
public class ScopusOutputResult {
    
    @XmlElement(name = "atom:entry")
    List<ScopusOutput> results;
    
    public List<ScopusOutput> results() {
        return results;
    }
}
