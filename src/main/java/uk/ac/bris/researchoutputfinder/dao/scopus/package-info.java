@XmlSchema(
        xmlns={
            @XmlNs(prefix = "atom", namespaceURI = "http://www.w3.org/2005/Atom"),
            @XmlNs(prefix = "dce", namespaceURI ="http://purl.org/dc/elements/1.1/"),
            @XmlNs(prefix = "prism", namespaceURI = "http://prismstandard.org/namespaces/basic/2.0/"),
            @XmlNs(prefix = "sarticle", namespaceURI ="http://www.elsevier.com/xml/svapi/article/dtd")
        }
)
package uk.ac.bris.researchoutputfinder.dao.scopus;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;

