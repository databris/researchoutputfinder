package uk.ac.bris.researchoutputfinder.dao.scopus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class ScopusOutput {
    
    @XmlElement(name = "dce:title")
    public String title;
    
    @XmlElement(name = "prism:doi")
    public String doi;
    
    @XmlElement(name = "dce:identifier")
    public String identifier;
    
    @XmlElement(name = "prism:coverDate")
    public String coverDate;
    
    @XmlElement(name = "atom:eid")
    public String eid;
    
    @XmlElement(name = "prism:aggregationType")
    public String aggregationType;
    
    @XmlElement(name = "prism:publicationName")
    public String publicationName;
    
    @XmlElement(name = "atom:author")
    public List<ScopusAuthor> authors = new ArrayList<>();
    
    @XmlElement(name = "sarticle:openaccessArticle")
    public Boolean openaccessArticle;
    
    public String getTitle() {
        return title;
    }

    public String getDOI() {
        return doi;
    }
    
    public String getIdentifer() {
        // raw identifier is of the form SCOPUS_ID
        if (identifier == null) return "";
        
        return identifier.substring(10);
    }
    
    @XmlRootElement(name = "sarticle:full-text-retrieval-response")
    public static class FullTextWrapper {
        @XmlElement(name = "sarticle:coredata")   
        public ScopusOutput output;
    }
}
