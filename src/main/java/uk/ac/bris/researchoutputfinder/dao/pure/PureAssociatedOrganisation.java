package uk.ac.bris.researchoutputfinder.dao.pure;

import org.eclipse.persistence.oxm.annotations.XmlPath;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class PureAssociatedOrganisation {
    @XmlPath("period/startDate/text()")
    String startDate;
    
    @XmlPath("period/endDate/text()")
    String endDate;
    
    @XmlPath("organisationalUnit/name/text()")
    String name;
    
    @XmlPath("organisationalUnit/type/@uri")
    String type;
    
    @XmlPath("emails/email/text()")
    String email;
}
