package uk.ac.bris.researchoutputfinder.dao.pubmed;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.persistence.oxm.annotations.XmlPath;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
@XmlRootElement(
        name = "eSearchResult"
)
public class PubMedSearchResult {
    
    @XmlPath("IdList/Id/text()")
    public List<Integer> ids = new ArrayList<>();
    
    @XmlElement(name = "Count")
    public int count;
    
    @XmlElement(name = "RetMax")
    public int retMax;
    
    @XmlElement(name = "RetStart")
    public int retStart;
    
}
