package uk.ac.bris.researchoutputfinder.dao.pubmed;

import javax.xml.bind.annotation.XmlElement;
import org.eclipse.persistence.oxm.annotations.XmlPath;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class PubMedAuthor {
    
    @XmlElement(name = "ForeName")
    public String forename;
    @XmlElement(name = "LastName")
    public String surname;
    @XmlPath(value = "AffiliationInfo/Affiliation/text()")
    public String affilitation;
    
}
