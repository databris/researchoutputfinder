package uk.ac.bris.researchoutputfinder.dao.pure;

import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import org.eclipse.persistence.oxm.annotations.XmlPath;

/**
 *
 * @author pldms
 */
public class PurePerson {
    
    final static String ACADEMIC_CLASS = "/dk/atira/pure/organisation/organisationtypes/organisation/academic"; 
    
    @XmlAttribute(name = "uuid")
    public String id;
    
    @XmlPath("name/firstName/text()")
    public String firstName;
    
    @XmlPath("name/lastName/text()")
    public String lastName;
    
    @XmlPath("staffOrganisationAssociations/staffOrganisationAssociation")
    public List<PureAssociatedOrganisation> orgs;
    
    @Override
    public String toString() {
        return String.format(
                "PurePerson(%s,'%s','%s','%s','%s')",
                id, firstName, lastName, getAcademicOrg(), getEmail()
        );
    }
    
    public String getName() {
        return lastName + ", " + firstName;
    }
    
    public String getAcademicOrg() {
        if (orgs == null) return "";
        
        // Get first org with no end date and type academic class
        return orgs.stream()
                .filter(org -> org.endDate == null && org.type != null && org.type.startsWith(ACADEMIC_CLASS))
                .map(org -> org.name)
                .findFirst()
                .orElse("");
    }
    
    public String getEmail() {
        if (orgs == null) return "";
        
        // Get first email
        return orgs.stream()
                .filter(org -> org.email != null)
                .map(org -> org.email)
                .findFirst()
                .orElse("");
    }
}
