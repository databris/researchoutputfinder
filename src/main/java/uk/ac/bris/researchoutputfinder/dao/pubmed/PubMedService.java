package uk.ac.bris.researchoutputfinder.dao.pubmed;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class PubMedService {
    
    private final Client client;
    private final URI serviceBase;
    
    public PubMedService(URI serviceBase) {
        this.client = ClientBuilder.newClient();
        
        this.serviceBase = serviceBase;
    }
    
    public PubMedSearchResult findOutputsWithAffiliationFromIndex(
            String affilQuery, int start, int max) {
        
        WebTarget target = client.target(serviceBase.resolve("esearch.fcgi"));
        
        Invocation inv = target
                .queryParam("db", "pubmed")
                .queryParam("retstart", start)
                .queryParam("retmax", max)
                .queryParam("term", affilQuery + "[Affiliation]")
                .request(MediaType.APPLICATION_XML_TYPE)
                .buildGet();
        
        return inv.invoke(PubMedSearchResult.class);
    }
    
    public PubMedFetchResult getOutputsWithIds(List<Integer> ids) {
        
        WebTarget target = client.target(serviceBase.resolve("efetch.fcgi"));
        
        String idQuery = ids.stream()
                .map(id -> id.toString())
                .collect(Collectors.joining(","));
        
        Invocation inv = target
                .queryParam("db", "pubmed")
                .queryParam("retmode", "xml")
                .queryParam("id", idQuery)
                .request(MediaType.APPLICATION_XML_TYPE)
                .buildGet();
        
        return inv.invoke(PubMedFetchResult.class);
    }
}
