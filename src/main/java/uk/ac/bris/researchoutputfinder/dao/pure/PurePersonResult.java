package uk.ac.bris.researchoutputfinder.dao.pure;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.persistence.oxm.annotations.XmlPath;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
@XmlRootElement(
    name = "result"
)
public class PurePersonResult {
    
    @XmlPath("person")
    public List<PurePerson> results;
    
    @XmlElement
    int count;
    
    @Override
    public String toString() {
        return String.format("PurePersonResult(%d,%s)", count, results);
    }
    
}
