package uk.ac.bris.researchoutputfinder.dao.pure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
@XmlRootElement(name = "result")
public class PureOutputResult {

    @XmlElements({
        @XmlElement(name = "researchOutput"),
        @XmlElement(name = "bookAnthology"),
        @XmlElement(name = "contributionToBookAnthology"),
        @XmlElement(name = "contributionToConference"),
        @XmlElement(name = "contributionToJournal"),
        @XmlElement(name = "contributionToMemorandum"),
        @XmlElement(name = "contributionToPeriodical"),
        @XmlElement(name = "memorandum"),
        @XmlElement(name = "nonTextual"),
        @XmlElement(name = "otherContribution"),
        @XmlElement(name = "patent"),
        @XmlElement(name = "thesis"),
        @XmlElement(name = "workingPaper")
    })
    public List<PureOutput> results = new ArrayList<>();

    @XmlElement
    int count;
}
