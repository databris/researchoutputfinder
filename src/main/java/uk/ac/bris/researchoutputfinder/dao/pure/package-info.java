@XmlSchema(
        xmlns={
            @XmlNs(prefix = "core", namespaceURI = "http://atira.dk/schemas/pure4/model/core/current"),
            @XmlNs(prefix = "cur1", namespaceURI = "http://atira.dk/schemas/pure4/model/template/abstractpublication/current"),
            @XmlNs(prefix = "cur", namespaceURI = "http://atira.dk/schemas/pure4/model/template/abstractperson/current"),
            @XmlNs(prefix = "extensions-core", namespaceURI = "http://atira.dk/schemas/pure4/model/core/extensions/current"),
            @XmlNs(prefix = "organisation-template", namespaceURI = "http://atira.dk/schemas/pure4/model/template/abstractorganisation/current")
        }
)
package uk.ac.bris.researchoutputfinder.dao.pure;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;

