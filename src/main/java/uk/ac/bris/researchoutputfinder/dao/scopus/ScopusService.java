package uk.ac.bris.researchoutputfinder.dao.scopus;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class ScopusService {

    private final Client client;
    private final URI serviceBase;
    private final String apiKey;

    public ScopusService(URI serviceBase, String apiKey) {
        this.client = ClientBuilder.newClient();

        this.serviceBase = serviceBase;
        this.apiKey = apiKey;
    }

    public ScopusOutputResult findOutputsWithAffiliationFromIndex(
            List<Integer> affiliationIds, int start, int max, String startDate) throws InterruptedException {

        String query = affiliationIds.stream()
                .map(i -> i.toString())
                .collect(Collectors.joining(") or af-id(", "af-id(", ")"));
        
        // limit query to after load DATE
        // "AND orig-load-date aft 20160530"
        // See https://dev.elsevier.com/tecdoc_ir_cris_vivo.html
        
        //query += " and orig-load-date aft " + startDate;
        
        query = "orig-load-date > " + startDate + " and (" + query + ")";
        
        System.out.println("Query is: " + query);

        WebTarget target = client.target(serviceBase.resolve("content/search/scopus"));

        Invocation inv = target
                .queryParam("query", query)
                .queryParam("field", "title,doi,identifier,author,afid,coverDate,eid,aggregationType,publicationName")
                .queryParam("start", start)
                .queryParam("count", max)
                // DON'T SORT! It messes with the natural ordering, which seems to be
                // addition order
                //.queryParam("sort", "orig-load-date,creator")
                .request(MediaType.APPLICATION_XML_TYPE)
                .header("X-ELS-APIKey", apiKey)
                .header("User-Agent", "OA Compliance tool - d.steer@bris.ac.uk")
                .buildGet();

        return invoke(inv, ScopusOutputResult.class);
    }

    public ScopusOutput getArticleInfoByEID(String eid) throws InterruptedException {

        WebTarget target = client.target(serviceBase.resolve("content/article/eid/" + eid));

        Invocation inv = target
                .queryParam("field", "openaccessArticle")
                .request(MediaType.APPLICATION_XML_TYPE)
                .header("X-ELS-APIKey", apiKey)
                .header("User-Agent", "OA Compliance tool - d.steer@bris.ac.uk")
                .buildGet();

        return invoke(inv, ScopusOutput.FullTextWrapper.class).output;
    }
    
    public ScopusOutputResult findOutputsWithDOIs(
            List<String> dois) throws InterruptedException {

        String query = dois.stream()
                .map(i -> i.toString())
                .collect(Collectors.joining(") or doi(", "doi(", ")"));

        System.out.println("Query is: " + query);

        WebTarget target = client.target(serviceBase.resolve("content/search/scopus"));

        Invocation inv = target
                .queryParam("query", query)
                .queryParam("field", "title,doi,identifier,author,afid,coverDate,eid")
                // DON'T SORT! It messes with the natural ordering, which seems to be
                // addition order
                //.queryParam("sort", "orig-load-date,creator")
                .request(MediaType.APPLICATION_XML_TYPE)
                .header("X-ELS-APIKey", apiKey)
                .header("User-Agent", "OA Compliance tool - d.steer@bris.ac.uk")
                .buildGet();

        return invoke(inv, ScopusOutputResult.class);
    }
    
    private <T> T invoke(Invocation inv, Class<T> klass) throws InterruptedException {
        try {
            Thread.sleep(7000); // try not being eagar
            return inv.invoke(klass);
        } catch (ClientErrorException e) {
            if (e.getResponse().getStatus() == 429) {
                System.err.print("# Rate limit reached - sleeping for 30 minutes then retrying");
                
                Thread.sleep(30 * 60000);
                
                return inv.invoke(klass);
            } else {
                System.err.print("# Issue with scopus retrying in 30s: " + e.getMessage());
                Thread.sleep(30000);
                
                return inv.invoke(klass);
            }
        }
    }

}
