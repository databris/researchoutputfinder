package uk.ac.bris.researchoutputfinder.dao.scopus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class ScopusAuthor {
    @XmlElement(name = "atom:given-name")
    public String givenName;
    
    @XmlElement(name = "atom:surname")
    public String surname;
    
    @XmlElement(name = "atom:authid")
    public String identifier;
    
    @XmlElement(name = "atom:afid")
    public List<Integer> affiliationIds = new ArrayList<>();   
}
