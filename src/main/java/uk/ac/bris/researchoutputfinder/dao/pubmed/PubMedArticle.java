package uk.ac.bris.researchoutputfinder.dao.pubmed;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import org.eclipse.persistence.oxm.annotations.XmlPath;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class PubMedArticle {
    
    @XmlElement(name = "ArticleTitle")
    public String title;
    
    @XmlPath("ELocationID[@EIdType='doi']/text()")
    public String doi;
    
    @XmlPath("AuthorList/Author")
    public List<PubMedAuthor> authors;
    
    @XmlPath("ArticleDate/Year/text()")
    public String year;
    
    @XmlPath("ArticleDate/Month/text()")
    public String month;
    
    @XmlPath("ArticleDate/Day/text()")
    public String day;
    
    @XmlPath("PublicationTypeList/PublicationType/text()")
    public String publicationType;
    
    @XmlPath("Journal/Title/text()")
    public String sourceTitle;
    
    public String articleDate() {
        if (year == null) return "unknown";
        return String.format("%s-%s-%s", year, month, day);
    }
    
}
