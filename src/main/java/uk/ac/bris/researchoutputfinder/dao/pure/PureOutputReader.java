package uk.ac.bris.researchoutputfinder.dao.pure;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

/**
 * 
 * PureOutput corresponds to many xml root elements, which JAXB can't really represent
 * 
 * This is a workaround where we explicitly handle the response.
 * 
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
@Provider
public class PureOutputReader implements MessageBodyReader<PureOutput> {

    private final Unmarshaller unmarshaller;
    
    public PureOutputReader() throws JAXBException {
        // Explicit context for this class. Seems to do the trick
        JAXBContext jc = JAXBContext.newInstance(PureOutput.class);
        unmarshaller = jc.createUnmarshaller();
    }
    
    @Override
    public boolean isReadable(Class<?> type, Type type1, Annotation[] antns, MediaType mt) {
        return MediaType.APPLICATION_XML_TYPE.equals(mt) 
                && PureOutput.class.isAssignableFrom(type);
    }

    @Override
    public PureOutput readFrom(Class<PureOutput> type, Type type1, Annotation[] antns, MediaType mt, MultivaluedMap<String, String> mm, InputStream in) throws IOException, WebApplicationException {
        try {
            // Be explicit again
            JAXBElement<PureOutput> result = 
                    unmarshaller.unmarshal(new StreamSource(in), PureOutput.class);
            return result.getValue();
        } catch (JAXBException ex) {
            throw new RuntimeException(ex);
        }
    }
    
}
