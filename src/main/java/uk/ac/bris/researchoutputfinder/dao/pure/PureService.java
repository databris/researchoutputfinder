package uk.ac.bris.researchoutputfinder.dao.pure;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class PureService {

    private final Client client;
    private final URI serviceBase;
    private final String apiKey;
    
    public PureService(URI serviceBase, String apiKey) {
        this.client = ClientBuilder.newClient();
        
        client.register(PureOutputReader.class);
        
        this.serviceBase = serviceBase;
        this.apiKey = apiKey;
    }
    
    public List<PureOutput> findOutputById(String id, String idType) {
        
        WebTarget target = client.target(serviceBase).path("research-outputs");
        
        // DOI search is naughty = '/' is not encoded
        // so we need to do a little dance
        String[] components = id.split("/");
        for (String component: components) {
            target = target.path(component);
        }
        
        target = target.queryParam("idClassification", idType);
        
        //System.out.printf("Target is <%s>\n", target);
        
        Invocation inv = target
                .request(MediaType.APPLICATION_XML_TYPE)
                .header("api-key", apiKey)
                .buildGet();
        
        // Ideally we'd return an optional, but this is very similar to
        // name search, so code is ultimately tidier.
        try {
            PureOutput result = inv.invoke(PureOutput.class);
            if (result == null) {
                return Collections.EMPTY_LIST;
            } else {
                return Collections.singletonList(result);
            }
        } catch (NotFoundException e) {
            return Collections.EMPTY_LIST;
        }
    }
    
    public List<PureOutput> findOutputFromTitle(String title, String doi) {
        
        WebTarget target = client.target(serviceBase).path("research-outputs");
        
        String search = escapeSearch(title);
        
        Invocation inv = target
                .queryParam("q", search)
                .queryParam("fields", "title,electronicVersions.*,externalableInfo.*")
                .request(MediaType.APPLICATION_XML_TYPE)
                .header("api-key", apiKey)
                .buildGet();
        
        //System.out.println("Query string: " + search);
        PureOutputResult result;
        try {
            result = inv.invoke(PureOutputResult.class);
        } catch (InternalServerErrorException e) {
            System.err.printf("Error searching for title '%s'\n", search);
            System.err.println(e);
            return Collections.EMPTY_LIST;
        }
        
        Optional<PureOutput> doiMatch = Optional.empty();
        
        // If we're not looking for a DOI, and only have one hit
        if (result.count == 1 && (doi == null || doi.isEmpty())) {
            doiMatch = Optional.of(result.results.get(0));
        } else {
            doiMatch = result.results.stream()
                //.peek(po -> System.out.println("I have: " + po.title))
                .filter(po -> po.doi != null)
                .filter(po -> po.doi.equals("http://dx.doi.org/" + doi))
                .findAny();
        }
        
        if (doiMatch.isPresent()) {
            return Collections.singletonList(doiMatch.get());
        } else {
            return result.results;
        }
    }
    
    public List<PurePerson> findPersonFromName(String givenName, String surname) {
                
        WebTarget target = client.target(serviceBase).path("persons");
        
        String firstname = stripInitials(givenName);
        
        String query = (firstname.isEmpty()) ?
                surname : firstname + " " + surname;

        //System.out.println("Query: " +query);
        
        Invocation inv = target
                .queryParam("q", query)
                .request(MediaType.APPLICATION_XML_TYPE)
                .header("api-key", apiKey)
                .buildGet();
        
        PurePersonResult res;
        
        try {
            res = inv.invoke(PurePersonResult.class);
        } catch (InternalServerErrorException e) {
            System.err.printf("Error searching for person '%s'\n", query);
            System.err.println(e);
            return Collections.EMPTY_LIST;
        }
        
        //System.err.printf("Query: '%s', Result: '%s'\n", query, res);
        
        if (res.results == null) return Collections.EMPTY_LIST;
        else return res.results;
    }
    
    final static Pattern INITIAL_MATCH = Pattern.compile("[\\.A-Z]+(\\s|$)");
    
    /**
     * Remove anything that looks like initials from a name
     * 
     * @param name
     * @return name minus initials (hopefully)
     */
    public static String stripInitials(String name) {
        if (name == null) return "";
        String res = INITIAL_MATCH.matcher(name).replaceAll("").trim();
        //System.err.printf("> In '%s', out '%s'\n", name, res);
        return res;
    }
    
    final static Pattern ESC = //Pattern.compile("([+\\-&\\|!(){}\\[\\]^\"~*?:\\\\])");
            Pattern.compile("([\\[\\]/])");
    
    static String escapeSearch(String in) {
        return ESC.matcher(in).replaceAll("\\\\$1").replaceAll("\\{", "%7B").replaceAll("\\}", "%7D");
    }
    
    public static void main(String[] args) {
                
        PureService service = new PureService(
                URI.create("https://ris-b.services.bris.ac.uk/ws/api/513/"),
                ""
        );
        
        
        System.out.println(service.findPersonFromName("Damian", "Steer"));
        
        System.out.println(service.findOutputFromTitle(
                "Real-time, portable genome sequencing for Ebola surveillance", 
                "10.1038/nature16996"));
        
        // A book "Richard Cantillon; Pioneer of Economic Theory"
        System.out.println(service.findOutputFromTitle(
                "Richard Cantillon; Pioneer of Economic Theory", 
                ""));
        
        // PubMed
        System.out.println(service.findOutputById("26840485", "source"));
        
        // Not exist
        System.out.println(service.findOutputById("9x9x9x9x9", "source"));
        
        // Scopus - not article!
        System.out.println(service.findOutputById("84862063453", "source"));
        
        // DOI - I will find you!
        System.out.println(service.findOutputById("10.1038/nature16996", "doi"));
        
        // DOI - not there
        System.out.println(service.findOutputById("10.1097/SLA.0000000000003106", "doi"));
        
        // Person
        System.out.print(service.findPersonFromName("Damian", "Steer"));
    }
}
