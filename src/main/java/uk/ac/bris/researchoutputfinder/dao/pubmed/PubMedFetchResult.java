package uk.ac.bris.researchoutputfinder.dao.pubmed;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
@XmlRootElement(
        name = "PubmedArticleSet"
)
public class PubMedFetchResult {
    
    @XmlElement(name = "PubmedArticle/MedlineCitation/Article")
    public List<PubMedArticle> articles = new ArrayList<>();
    
}
