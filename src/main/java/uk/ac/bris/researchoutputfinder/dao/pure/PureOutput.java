package uk.ac.bris.researchoutputfinder.dao.pure;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.oxm.annotations.XmlPath;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class PureOutput {
    
    @XmlAttribute(name = "uuid")
    public String id;
    
    @XmlElement
    public String title;
    
    @XmlPath("electronicVersions/electronicVersion/doi/text()")
    public String doi;
    
    @XmlElement(name = "externalableInfo")
    public PureExternalId externalId;
    
    @Override public String toString() {
        
        // Just be careful, especially in toString
        String sourceId = (externalId == null) ? null : externalId.sourceId;
        
        return String.format(
                "PureOutput(%s,%s,%s,'%s')", 
                id, doi, sourceId, StringUtils.abbreviate(title, 20)  
        );
    }
    
    public static class PureExternalId {
        
        @XmlElement(name = "source")
        public String source;
        
        @XmlElement(name = "sourceId")
        public String sourceId;
        
    }
    
}
