package uk.ac.bris.researchoutputfinder;

import com.opencsv.CSVWriter;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import uk.ac.bris.researchoutputfinder.dao.datahub.DatahubService;
import uk.ac.bris.researchoutputfinder.dao.pure.PureOutput;
import uk.ac.bris.researchoutputfinder.dao.pure.PureOutputResult;
import uk.ac.bris.researchoutputfinder.dao.pure.PurePerson;
import uk.ac.bris.researchoutputfinder.dao.pure.PurePersonResult;
import uk.ac.bris.researchoutputfinder.dao.pure.PureService;
import uk.ac.bris.researchoutputfinder.dao.scopus.ScopusAuthor;
import uk.ac.bris.researchoutputfinder.dao.scopus.ScopusOutput;
import uk.ac.bris.researchoutputfinder.dao.scopus.ScopusOutputResult;
import uk.ac.bris.researchoutputfinder.dao.scopus.ScopusService;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class ScoupusFindSchoolFromDOIs {

    public static void main(String[] args) throws InterruptedException, IOException, SQLException {

        // All UoB
        List<Integer> affiliations
                = Arrays.asList(60020650, 60014311, 60013426, 60101499);

        ScopusService ss = new ScopusService(
                URI.create("https://api.elsevier.com/"),
                "3f1f7e5c5dcdbc704067f499189eb75d"
        );

        PureService pure = new PureService(
                URI.create("https://ris-b.services.bris.ac.uk/ws/api/510/"),
                ""
        );

        DatahubService datahub = new DatahubService();

        Path outputFile = Paths.get("/tmp/doi2school.csv");

        List<String> allDOIs = Files.readAllLines(Paths.get("/tmp/DOI-to-school.txt"));

        CSVWriter writer = new CSVWriter(Files.newBufferedWriter(outputFile));

        writer.writeNext(new String[]{"doi", "author", "school"});

        int chunkSize = 20;

        for (int i = 0; i < allDOIs.size(); i += chunkSize) {
            int end = Math.min(allDOIs.size(), i + chunkSize);
            getSchools(ss, pure, datahub, allDOIs.subList(i, end), writer, affiliations);
        }

        writer.flush();
        writer.close();
    }

    public static void getSchools(ScopusService ss, PureService pure, DatahubService datahub, List<String> dois, CSVWriter writer, List<Integer> affiliations)
            throws InterruptedException, IOException {

        ScopusOutputResult results
                = ss.findOutputsWithDOIs(dois);

        for (ScopusOutput result : results.results()) {
            
            if (pure.findOutputById(result.getIdentifer(), "scopus") != null) {
                System.out.print("+ ");
            } else {
                System.out.print("! ");
            }

            List<ScopusAuthor> uobScopusAuthors = result.authors.stream()
                    .filter(auth -> !Collections.disjoint(auth.affiliationIds, affiliations))
                    //.map(auth -> pure.findPersonFromName(auth.givenName, auth.surname))
                    .collect(Collectors.toList());

            String[] row = new String[3];

            row[0] = result.getDOI();

            if (uobScopusAuthors.isEmpty()) {
                row[1] = "No UoB authors found (?)";
            } else {
                boolean foundPureAuthor = false;

                for (ScopusAuthor scopusAuthor : uobScopusAuthors) {
                    List<PurePerson> pureAuthor
                            = pure.findPersonFromName(scopusAuthor.givenName, scopusAuthor.surname);

                    if (!pureAuthor.isEmpty()) {

                        foundPureAuthor = true;

                        row[1] = pureAuthor.get(0).getName();
                        row[2] = pureAuthor.get(0).getAcademicOrg();

                        break;
                    }
                }

                if (!foundPureAuthor) {
                    row[1] = "(No pure author)" + uobScopusAuthors.get(0).surname + ", " + uobScopusAuthors.get(0).givenName;
                    row[2] = "";
                }

                writer.writeNext(row);
                writer.flush();
            }

            System.out.printf("(%s) %s\t%s\t%s\t%s\n",
                    result.coverDate,
                    result.getIdentifer(),
                    result.getDOI(),
                    "https://api.elsevier.com/content/article/eid/" + result.eid,
                    result.title);
        }
    }
}
