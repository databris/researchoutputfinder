package uk.ac.bris.researchoutputfinder;

import com.opencsv.CSVWriter;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;
import uk.ac.bris.researchoutputfinder.dao.pubmed.PubMedArticle;
import uk.ac.bris.researchoutputfinder.dao.pubmed.PubMedAuthor;
import uk.ac.bris.researchoutputfinder.dao.pubmed.PubMedFetchResult;
import uk.ac.bris.researchoutputfinder.dao.pubmed.PubMedSearchResult;
import uk.ac.bris.researchoutputfinder.dao.pubmed.PubMedService;
import uk.ac.bris.researchoutputfinder.dao.pure.PureOutput;
import uk.ac.bris.researchoutputfinder.dao.pure.PurePerson;
import uk.ac.bris.researchoutputfinder.dao.pure.PureService;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
public class PubMed {

    final static String PURE_PERSON_LINK = "http://research-information.bristol.ac.uk/en/persons/dummy(%s).html";

    public static void main(String[] args) throws IOException, SQLException {
        
        Properties config = Config.load();
        
        PubMedService service = new PubMedService(
                URI.create(config.getProperty("pubmed.endpoint"))
        );

        PureService pure = new PureService(
                URI.create(config.getProperty("pure.endpoint")),
                config.getProperty("pure.apikey")
        );
        
        //DatahubService pure = new DatahubService();
        
        Path outputFile = Paths.get("/tmp/pubmed.csv");
        Path lastIdFile = Paths.get("pubmed_lastid.txt");
        Path lastIdFileBackup = Paths.get("pubmed_lastid_previous.txt");
        
        String lastId;
        
        if (Files.exists(lastIdFile)) {
            Files.copy(lastIdFile, lastIdFileBackup, StandardCopyOption.REPLACE_EXISTING);
            lastId = Files.readString(lastIdFile).trim();
        } else {
            System.out.printf("Last id file <%d> not found\n", lastIdFile.toAbsolutePath());
            System.exit(1);
            return; // make compiler happy
        }
        
        System.out.printf("Last id is <%s>\n", lastId);
        
        CSVWriter writer = new CSVWriter(Files.newBufferedWriter(outputFile));

        writer.writeNext(new String[]{"doi", "date", "title", "source type", "source title", "author", "affliliation",
            "pure author", "school", "pure url", "email"});

        final int pageSize = 50;
        int start = 0;
        
        Optional<String> firstId = Optional.empty();
        
        while (start < 1000) {

            System.out.println("Start: " + start);

            PubMedSearchResult outputsSearch
                    = service.findOutputsWithAffiliationFromIndex("university of bristol", start, pageSize);
                        
            start += outputsSearch.ids.size();
            
            PubMedFetchResult outputs = service.getOutputsWithIds(outputsSearch.ids);
            
            if (firstId.isEmpty() && outputs.articles.size() > 0) {
                firstId = Optional.of(outputs.articles.get(0).doi);
            }
            
            boolean finished = handleOutputs(outputs, pure, writer, lastId);
            
            if (finished) {
                System.out.println("Found lastId, exiting..."); 
                break;
            }
        }

        writer.flush();
        writer.close();
                
        if (firstId.isPresent()) {
            Files.writeString(lastIdFile, firstId.get());
        } else {
            System.out.println("No ids found");
        }
    }

    private static boolean handleOutputs(PubMedFetchResult outputs, PureService pure, CSVWriter writer, String lastId) throws SQLException {
        for (PubMedArticle output : outputs.articles) {
            
            System.out.println("Handle: " + output.doi);
            
            // We're done
            if (lastId.equals(output.doi)) return true;
            
            List<PureOutput> articles = (output.doi == null) ?
                    pure.findOutputFromTitle(output.title, output.doi) :
                    pure.findOutputById(output.doi, "doi") ;
                    
            if (!articles.isEmpty()) {
                // We have the article already?
                System.out.println("GOT THIS ONE!");
                continue;
            }
            
            String[] row = new String[11];

            row[0] = output.doi;
            row[1] = output.articleDate();
            row[2] = output.title;
            row[3] = output.publicationType;
            row[4] = output.sourceTitle;
            
            List<PubMedAuthor> brisAuthors = output.authors.stream()
                    .filter(author -> author.affilitation != null)
                    .filter(author -> author.affilitation.contains("University of Bristol"))
                    .collect(Collectors.toList());

            if (brisAuthors.isEmpty()) {
                System.err.println("No bristol authors for " + output.doi);
                row[5] = "No UoB authors found (?)";
            }

            for (PubMedAuthor author : brisAuthors) {
                
                //System.out.printf("\t'%s' '%s' (%s)\n",
                //        author.forename, author.surname, author.affilitation);

                List<PurePerson> people = pure.findPersonFromName(author.forename, author.surname);

                if (people.isEmpty()) {
                    // we already have something a bit better, so skip it
                    if (row[5] != null) continue;
                    
                    row[5] = author.surname + ", " + author.forename;
                    row[6] = author.affilitation;
                    row[7] = "NOT FOUND";
                } else if (people.size() > 1) {
                    // we already have something a bit better, so skip it
                    if (row[7] != null && !row[7].equals("NOT FOUND")) continue;
                    
                    row[5] = author.surname + ", " + author.forename;
                    row[6] = author.affilitation;
                    row[7] = "FOUND " + people.size() + " PEOPLE";
                    row[9] = people.stream()
                            .map(PurePerson::getEmail)
                            .collect(Collectors.joining(" , "));
                } else {
                    row[5] = author.surname + ", " + author.forename;
                    row[6] = author.affilitation;
                    row[7] = people.get(0).getName();
                    row[8] = people.get(0).getAcademicOrg();
                    row[9] = String.format(PURE_PERSON_LINK, people.get(0).id);
                    row[10] = people.get(0).getEmail();
                    // This is as good as it gets - use this row
                    break;
                }
            }
            
            writer.writeNext(row);
        }
        
        return false;
    }
}
