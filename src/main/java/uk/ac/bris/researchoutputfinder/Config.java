package uk.ac.bris.researchoutputfinder;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 *
 * @author Damian Steer <d.steer@bris.ac.uk>
 */
class Config {
    
    public static Properties load() {
        Properties properties = new Properties();
        
        Path propFile = Paths.get("config", "application.properties");
        
        try (InputStream in = Files.newInputStream(propFile)) {
            properties.load(in);
            return properties;
        } catch (IOException ex) {
            throw new RuntimeException("Issue loading property file " + propFile, ex);
        }
    }
    
}
